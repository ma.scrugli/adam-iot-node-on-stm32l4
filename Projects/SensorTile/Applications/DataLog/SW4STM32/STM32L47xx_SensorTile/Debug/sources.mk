################################################################################
# Automatically-generated file. Do not edit!
################################################################################

C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
OBJ_SRCS := 
S_SRCS := 
CC_SRCS := 
ASM_SRCS := 
C_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
CC_DEPS := 
C++_DEPS := 
EXECUTABLES := 
OBJS := 
C_UPPER_DEPS := 
CXX_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Application/User \
CMSIS/DSP/BasicMathFunctions \
CMSIS/DSP/CommonTables \
CMSIS/DSP/ComplexMathFunctions \
CMSIS/DSP/ControllerFunctions \
CMSIS/DSP/FastMathFunctions \
CMSIS/DSP/FilteringFunctions \
CMSIS/DSP/MatrixFunctions \
CMSIS/DSP/StatisticsFunctions \
CMSIS/DSP/SupportFunctions \
CMSIS/DSP/TransformFunctions \
CMSIS/NN/ActivationFunctions \
CMSIS/NN/ConvolutionFunctions \
CMSIS/NN/FullyConnectedFunctions \
CMSIS/NN/NNSupportFunctions \
CMSIS/NN/PoolingFunctions \
CMSIS/NN/SoftmaxFunctions \
Drivers/BSP/Components \
Drivers/BSP/SensorTile \
Drivers/CMSIS \
Drivers/STM32L4xx_HAL_Driver \
Example/SW4STM32 \
MLKWS/KWS/KWS_DNN \
MLKWS/KWS/KWS_DS_CNN \
MLKWS/KWS \
MLKWS/KWS_F746NG \
MLKWS/MFCC \
MLKWS/NN/DNN \
MLKWS/NN/DS_CNN \
MLKWS/NN \
MLKWS/local_NN \
Middlewares/FatFs/Core \
Middlewares/FatFs/Drivers \
Middlewares/FatFs/Options \
Middlewares/FreeRTOS/CMSIS_RTOS \
Middlewares/FreeRTOS \
Middlewares/FreeRTOS/portable \
Middlewares/STM32_BlueNRG/HCI/Controller \
Middlewares/STM32_BlueNRG/HCI \
Middlewares/STM32_BlueNRG/Interface \
Middlewares/STM32_BlueNRG/Utils \
Middlewares/STM32_USBD_Library/Class/CDC \
Middlewares/STM32_USBD_Library/Core \

